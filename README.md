﻿# VISION PRODUIT

POUR des moyennes entreprises 

QUI SOUHAITENT un réseau d’entreprise fonctionnel sécurisé et superviser les serveurs (composants type disque dur etc. et l'état de leurs services)

NOTRE PRODUIT EST une mise en place d'une infrastrustrure réseau, 4 machines dont 2 serveurs (1 Nagios, 1 FTP + WEB)

QUI permet d’avoir un réseau avec des services (web et ftp) supervisés avec nagios (alertes par mails suite à des évènements de type disque dur saturé)
dans une architecture sécurisé via listes d'accès sécurisé sur les equipements réseaux 


A LA DIFFERENCE DE beaucoup de réseaux manquant de sécurité et de supervision sur l'ensemble des serveurs et de leurs services

PERMET D'avoir une infrastrustrure réseau sécurisé et sous constante surveillance 


Répartition des tâches :

Kevin : Configuration de Nagios
Vincent : Configuration du réseau
Valentin et Tommy : Rédaction de la doc technique

POINTS DU PROJET

Total de points : 30

10 Points prévision initiale / demie-journée

3 sprint nécessaire pour terminer le projet.

Le projet devrait être terminé en 1,5 jour.

À l'issue du premier sprint, nous pouvons confirmer que la capacité de 10 points par demie-journée que nous avons défini a été respectée.

À l'issue du deuxième sprint, nous avons respecté une nouvelle fois la capacité de 10 points par demie-journée.